package com.prodemy.tugasyayan.repository;

import com.prodemy.tugasyayan.model.entity.Ujian;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UjianRepository extends JpaRepository<Ujian, Integer> {

}
