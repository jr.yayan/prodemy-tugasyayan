package com.prodemy.tugasyayan.repository;

import com.prodemy.tugasyayan.model.entity.Agama;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgamaRepository extends JpaRepository<Agama, Integer> {

}
