package com.prodemy.tugasyayan.repository;

import com.prodemy.tugasyayan.model.entity.TipeDosen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipeDosenRepository extends JpaRepository<TipeDosen, Integer> {

}
