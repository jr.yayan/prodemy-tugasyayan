package com.prodemy.tugasyayan.repository;

import com.prodemy.tugasyayan.model.entity.Jurusan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JurusanRepository extends JpaRepository<Jurusan, Integer> {

}
