package com.prodemy.tugasyayan.repository;

import com.prodemy.tugasyayan.model.entity.Dosen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DosenRepository extends JpaRepository<Dosen, Integer> {

}
