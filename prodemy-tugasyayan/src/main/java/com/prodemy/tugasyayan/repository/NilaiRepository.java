package com.prodemy.tugasyayan.repository;

import com.prodemy.tugasyayan.model.entity.Nilai;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NilaiRepository extends JpaRepository<Nilai, Integer> {

}
