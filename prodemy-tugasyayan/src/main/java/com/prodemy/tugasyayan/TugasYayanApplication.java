package com.prodemy.tugasyayan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TugasYayanApplication {

	public static void main(String[] args) {
	//System.setProperty("spring.config.name", "web-server");
		SpringApplication.run(TugasYayanApplication.class, args);
	}

}
