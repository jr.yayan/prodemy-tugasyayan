package com.prodemy.tugasyayan.service;

import com.prodemy.tugasyayan.model.entity.Nilai;
import com.prodemy.tugasyayan.repository.MahasiswaRepository;
import com.prodemy.tugasyayan.repository.NilaiRepository;
import com.prodemy.tugasyayan.repository.UjianRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class NIlaiServiceImpl implements com.prodemy.tugasyayan.service.NilaiService {
    @Autowired
    private NilaiRepository nilaiRepository;

    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private UjianRepository ujianRepository;

    @Override
    public Nilai saveNilaiMaterDetail(Nilai nilai) {
//        agamaRepository.save(mahasiswa.getAgama());
//        jurusanRepository.save(mahasiswa.getJurusan());
//        mahasiswaRepository.save(mahasiswa);
        nilai = nilaiRepository.save(nilai);
        nilai.setMahasiswa(mahasiswaRepository.findById(nilai.getIdMahasiswa()).get());
        nilai.setUjian(ujianRepository.findById(nilai.getIdUjian()).get());

        return nilai;
    }
}
