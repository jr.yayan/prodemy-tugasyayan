package com.prodemy.tugasyayan.service;

import com.prodemy.tugasyayan.model.entity.Dosen;

public interface DosenService {

    Dosen saveDosenMaterDetail(Dosen dosen);
}
