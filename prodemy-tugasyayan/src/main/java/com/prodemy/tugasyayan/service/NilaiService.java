package com.prodemy.tugasyayan.service;

import com.prodemy.tugasyayan.model.entity.Nilai;

public interface NilaiService {
    Nilai saveNilaiMaterDetail(Nilai nilai);
}
