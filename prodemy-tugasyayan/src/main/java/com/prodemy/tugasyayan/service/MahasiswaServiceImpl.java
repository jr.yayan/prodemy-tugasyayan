package com.prodemy.tugasyayan.service;

import com.prodemy.tugasyayan.model.entity.Mahasiswa;
import com.prodemy.tugasyayan.repository.AgamaRepository;
import com.prodemy.tugasyayan.repository.JurusanRepository;
import com.prodemy.tugasyayan.repository.MahasiswaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class MahasiswaServiceImpl implements MahasiswaService {
    @Autowired
    private MahasiswaRepository mahasiswaRepository;
    @Autowired
    private JurusanRepository jurusanRepository;
    @Autowired
    private AgamaRepository agamaRepository;

    @Override
    public Mahasiswa saveMahasiswaMaterDetail(Mahasiswa mahasiswa) {
//        agamaRepository.save(mahasiswa.getAgama());
//        jurusanRepository.save(mahasiswa.getJurusan());
//        mahasiswaRepository.save(mahasiswa);
        mahasiswa = mahasiswaRepository.save(mahasiswa);
        mahasiswa.setJurusan(jurusanRepository.findById(mahasiswa.getIdJurusan()).get());
        mahasiswa.setAgama(agamaRepository.findById(mahasiswa.getIdAgama()).get());

        return mahasiswa;
    }
}
