package com.prodemy.tugasyayan.service;

import com.prodemy.tugasyayan.model.entity.Mahasiswa;

public interface MahasiswaService {

    Mahasiswa saveMahasiswaMaterDetail(Mahasiswa mahasiswa);
}
