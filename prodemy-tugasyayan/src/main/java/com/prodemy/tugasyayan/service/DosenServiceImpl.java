package com.prodemy.tugasyayan.service;

import com.prodemy.tugasyayan.model.entity.Dosen;
import com.prodemy.tugasyayan.repository.DosenRepository;
import com.prodemy.tugasyayan.repository.JurusanRepository;
import com.prodemy.tugasyayan.repository.TipeDosenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Transactional
@Service
public class DosenServiceImpl implements DosenService {
    @Autowired
    private DosenRepository dosenRepository;
    @Autowired
    private JurusanRepository jurusanRepository;
    @Autowired
    private TipeDosenRepository tipeDosenRepository;

    @Override
    public Dosen saveDosenMaterDetail(Dosen dosen) {
//        jurusanRepository.save(dosen.getJurusan());
//        tipeDosenRepository.save(dosen.getTipeDosen());
        dosen = dosenRepository.save(dosen);
        dosen.setJurusan(jurusanRepository.findById(dosen.getIdJurusan()).get());
        dosen.setTipeDosen(tipeDosenRepository.findById(dosen.getIdTipeDosen()).get());
        return dosen;
    }
}
