package com.prodemy.tugasyayan.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = Ujian.TABLE_NAME)
@Data
public class Ujian {
    public static final String TABLE_NAME = "t_ujian";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ujian")
    private Integer id;
    private String ujian;
    private String statusUjian;
}
