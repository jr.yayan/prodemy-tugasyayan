package com.prodemy.tugasyayan.model.dto;

import lombok.Data;

@Data
public class NilaiDto {
    private Integer idNilai;
    private Integer idMahasiswa;
    private String mahasiswa;
    private Integer idUjian;
    private String ujian;
    private Integer nilai;
}
