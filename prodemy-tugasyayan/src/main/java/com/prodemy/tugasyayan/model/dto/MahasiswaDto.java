package com.prodemy.tugasyayan.model.dto;

import lombok.Data;


@Data
public class MahasiswaDto {
    private Integer idMahasiswa;
    private String mahasiswa;
    private String alamat;
    private Integer idAgama;
    private Integer idJurusan;
    private String agama;
    private String jurusan;
}
