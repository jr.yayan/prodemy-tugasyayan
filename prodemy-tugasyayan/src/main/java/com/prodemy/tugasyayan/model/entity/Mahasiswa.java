package com.prodemy.tugasyayan.model.entity;


import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = Mahasiswa.TABLE_NAME)
@Data
public class Mahasiswa {
    public static final String TABLE_NAME = "t_mahasiswa";

    @Id
    @GeneratedValue
    @Column(name = "id_mahasiswa")
    private Integer id;
    private String mahasiswa;
    private String alamat;

    @ManyToOne
    @JoinColumn(name = "id_agama", insertable = false, updatable = false)
    private Agama agama;

    @Column(name = "id_agama", nullable = false)
    private Integer idAgama;

    @ManyToOne
    @JoinColumn(name = "id_jurusan", insertable = false, updatable = false)
    private Jurusan jurusan;

    @Column(name = "id_jurusan", nullable = false)
    private Integer idJurusan;

}
