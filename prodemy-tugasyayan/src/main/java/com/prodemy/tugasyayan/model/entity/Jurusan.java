package com.prodemy.tugasyayan.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = Jurusan.TABLE_NAME)
@Data
public class Jurusan {
    public static final String TABLE_NAME = "t_jurusan";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_jurusan")
    private Integer id;
    private String jurusan;
    private String statusJurusan;

}
