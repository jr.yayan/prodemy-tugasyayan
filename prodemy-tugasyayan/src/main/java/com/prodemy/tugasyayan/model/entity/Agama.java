package com.prodemy.tugasyayan.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = Agama.TABLE_NAME)
@Data
public class Agama {
    public static final String TABLE_NAME = "t_agama";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_agama")
    private Integer id;
    private String agama;
}
