package com.prodemy.tugasyayan.model.dto;

import lombok.Data;

@Data
public class DosenDto {
    private Integer idDosen;
    private String dosen;
    private Integer idJurusan;
    private Integer idTipeDosen;
    private String jurusan;
    private String tipeDosen;

}
