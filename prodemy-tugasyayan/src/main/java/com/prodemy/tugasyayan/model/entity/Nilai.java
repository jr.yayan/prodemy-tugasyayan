package com.prodemy.tugasyayan.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = Nilai.TABLE_NAME)
@Data
public class Nilai {
    public static final String TABLE_NAME = "t_nilai";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_NAME)
    @SequenceGenerator(name = TABLE_NAME, sequenceName = "t_nilai_seq")

    @Column(name = "id_nilai")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "id_mahasiswa", insertable = false, updatable = false)
    private com.prodemy.tugasyayan.model.entity.Mahasiswa mahasiswa;
    @Column(name = "id_mahasiswa", nullable = false)
    private Integer idMahasiswa;

    @ManyToOne
    @JoinColumn(name = "id_ujian", insertable = false, updatable = false)
    private Ujian ujian;
    @Column(name = "id_ujian", nullable = false)
    private Integer idUjian;

    private Integer nilai;
}
