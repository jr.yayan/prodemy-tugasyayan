package com.prodemy.tugasyayan.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = Dosen.TABLE_NAME)
@Data
public class Dosen {
    public static final String TABLE_NAME = "t_dosen";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = TABLE_NAME)
    @SequenceGenerator(name = TABLE_NAME, sequenceName = "t_dosen_seq")

    @Column(name = "id_dosen")
    private Integer id;
    private String dosen;

    @ManyToOne
    @JoinColumn(name = "id_jurusan", insertable = false, updatable = false)
    private com.prodemy.tugasyayan.model.entity.Jurusan jurusan;

    @Column(name = "id_jurusan", nullable = false)
    private Integer idJurusan;

    @ManyToOne
    @JoinColumn(name = "id_tipe_dosen", insertable = false, updatable = false)
    private com.prodemy.tugasyayan.model.entity.TipeDosen tipeDosen;

    @Column(name = "id_tipe_dosen", nullable = false)
    private Integer idTipeDosen;
}
