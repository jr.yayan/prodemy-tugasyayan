package com.prodemy.tugasyayan.model.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = TipeDosen.TABLE_NAME)
@Data
public class TipeDosen {
    public static final String TABLE_NAME = "t_tipe_dosen";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipe_dosen")
    private Integer id;
    private String TipeDosen;
}
