package com.prodemy.tugasyayan.controller.restapi;

import com.prodemy.tugasyayan.model.dto.MahasiswaDto;
import com.prodemy.tugasyayan.model.entity.Mahasiswa;
import com.prodemy.tugasyayan.repository.MahasiswaRepository;
import com.prodemy.tugasyayan.service.MahasiswaService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/mahasiswa")
public class ApiMahasiswa {
    @Autowired
    private MahasiswaRepository mahasiswaRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private MahasiswaService mahasiswaService;

    @GetMapping()
    public List<MahasiswaDto> getListMahasiswa() {
        List<Mahasiswa> mahasiswaList = mahasiswaRepository.findAll();
        List<MahasiswaDto> mahasiswaDto =
                mahasiswaList.stream()
                             .map(mahasiswa -> mapMahaiswaToMahasiswaDto(mahasiswa))
                             .collect(Collectors.toList());
        return mahasiswaDto;
    }

    @GetMapping("/{id}")
    public MahasiswaDto getBiodata(@PathVariable Integer id) {
        Mahasiswa mahasiswa = mahasiswaRepository.findById(id).get();
        MahasiswaDto mahasiswaDto = new MahasiswaDto();
        modelMapper.map(mahasiswa, mahasiswaDto);
        modelMapper.map(mahasiswa.getAgama(), mahasiswaDto);
        modelMapper.map(mahasiswa.getJurusan(), mahasiswaDto);
//        mahasiswaDto.setIdAgama(mahasiswa.getAgama().getId());
//        mahasiswaDto.setIdJurusan(mahasiswa.getJurusan().getId());
        mahasiswaDto.setIdMahasiswa(mahasiswa.getId());
        return  mahasiswaDto;
    }

    @PostMapping()
    public MahasiswaDto editSaveData(@RequestBody MahasiswaDto mahasiswaDto) {
//        Agama agama = modelMapper.map(mahasiswaDto, Agama.class);
//        Jurusan jurusan = modelMapper.map(mahasiswaDto, Jurusan.class);
        Mahasiswa mahasiswa = modelMapper.map(mahasiswaDto, Mahasiswa.class);
        mahasiswa.setIdAgama(mahasiswaDto.getIdAgama());
        mahasiswa.setIdJurusan(mahasiswaDto.getIdJurusan());
        mahasiswa.setId(mahasiswaDto.getIdMahasiswa());
//        mahasiswa.setAgama(agama);
//        mahasiswa.setJurusan(jurusan);
        mahasiswa = mahasiswaService.saveMahasiswaMaterDetail(mahasiswa);
//        mahasiswaService.saveMahasiswaMaterDetail(mahasiswa);
        MahasiswaDto mahasiswaDtoDB = mapMahaiswaToMahasiswaDto(mahasiswa);
        return mahasiswaDtoDB;
    }

    private MahasiswaDto mapMahaiswaToMahasiswaDto(Mahasiswa mahasiswa) {
        MahasiswaDto mahasiswaDto = modelMapper.map(mahasiswa, MahasiswaDto.class);
        mahasiswaDto.setAgama(mahasiswa.getAgama().getAgama());
        mahasiswaDto.setJurusan(mahasiswa.getJurusan().getJurusan());
//        modelMapper.map(mahasiswa.getAgama(), mahasiswaDto);
//        modelMapper.map(mahasiswa.getJurusan(), mahasiswaDto);
        mahasiswaDto.setIdAgama(mahasiswa.getAgama().getId());
        mahasiswaDto.setIdJurusan(mahasiswa.getJurusan().getId());
        mahasiswaDto.setIdMahasiswa(mahasiswa.getId());
        return mahasiswaDto;
    }

    @DeleteMapping("/{idDel}")
    public void delete(@PathVariable Integer idDel) {
//        mahasiswaRepository.deleteById(Integer.parseInt(idDel));
        mahasiswaRepository.deleteById(idDel);
    }

    @DeleteMapping
    @ResponseBody
    public void deleteTable() {
        mahasiswaRepository.deleteAll();
    }
}
