package com.prodemy.tugasyayan.controller.restapi;

import com.prodemy.tugasyayan.model.entity.Jurusan;
import com.prodemy.tugasyayan.repository.JurusanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/jurusan")
public class ApiJurusan {
    @Autowired
    private JurusanRepository jurusanRepository;

    @GetMapping()
    public List<Jurusan> getAll() {
        return jurusanRepository.findAll();
    }

}
