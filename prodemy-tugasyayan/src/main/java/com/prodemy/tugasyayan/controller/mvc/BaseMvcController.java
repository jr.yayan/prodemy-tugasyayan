package com.prodemy.tugasyayan.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class BaseMvcController {

    @GetMapping("mahasiswa")
    public String mahasiswa() {
        return "tugas5/index"; }

    @GetMapping("dosen")
    public String dosen() {
        return "tugas5/index2";
    }

    @GetMapping("nilai")
    public String nilai() {
        return "tugas5/index3";
    }
}
