package com.prodemy.tugasyayan.controller.restapi;

import com.prodemy.tugasyayan.model.dto.NilaiDto;
import com.prodemy.tugasyayan.model.entity.Nilai;
import com.prodemy.tugasyayan.repository.NilaiRepository;
import com.prodemy.tugasyayan.service.NilaiService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/nilai")
public class ApiNilai {
    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private NilaiRepository nilaiRepository;

    @Autowired
    private NilaiService nilaiService;

    @GetMapping()
    public List<NilaiDto> getListMahasiswa() {
        List<Nilai> nilaiList = nilaiRepository.findAll();
        List<NilaiDto> nilaiDto =
                nilaiList.stream()
                        .map(nilai -> mapNilaiToNilaiDto(nilai))
                        .collect(Collectors.toList());
        return nilaiDto;
    }

    private NilaiDto mapNilaiToNilaiDto(Nilai nilai) {
        NilaiDto nilaiDto = modelMapper.map(nilai, NilaiDto.class);
        nilaiDto.setMahasiswa(nilai.getMahasiswa().getMahasiswa());
        nilaiDto.setUjian(nilai.getUjian().getUjian());
        nilaiDto.setIdMahasiswa(nilai.getMahasiswa().getId());
        nilaiDto.setIdUjian(nilai.getUjian().getId());
        nilaiDto.setIdNilai(nilai.getId());
        return nilaiDto;
    }

    @PostMapping()
    public NilaiDto editSaveData(@RequestBody NilaiDto nilaiDto) {
        Nilai nilai = modelMapper.map(nilaiDto, Nilai.class);
        nilai.setIdMahasiswa(nilaiDto.getIdMahasiswa());
        nilai.setIdUjian(nilaiDto.getIdUjian());
        nilai.setId(nilaiDto.getIdNilai());
        nilai = nilaiService.saveNilaiMaterDetail(nilai);
        NilaiDto nilaiDtoDB = mapNilaiToNilaiDto(nilai);
        return nilaiDtoDB;
    }

    @GetMapping("/{id}")
    public NilaiDto getBiodata(@PathVariable Integer id) {
        Nilai nilai = nilaiRepository.findById(id).get();
        NilaiDto nilaiDto = new NilaiDto();
        modelMapper.map(nilai, nilaiDto);
        modelMapper.map(nilai.getMahasiswa(), nilaiDto);
        modelMapper.map(nilai.getNilai(), nilaiDto);
        nilaiDto.setIdNilai(nilai.getId());
        return  nilaiDto;
    }

}
