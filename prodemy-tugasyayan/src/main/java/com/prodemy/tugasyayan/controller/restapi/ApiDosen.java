package com.prodemy.tugasyayan.controller.restapi;

import com.prodemy.tugasyayan.model.dto.DosenDto;
import com.prodemy.tugasyayan.model.entity.Dosen;
import com.prodemy.tugasyayan.repository.DosenRepository;
import com.prodemy.tugasyayan.service.DosenService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("api/dosen")
public class ApiDosen {
    @Autowired
    private DosenRepository dosenRepository;

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DosenService dosenService;

    @GetMapping()
    public List<DosenDto> getListDosen() {
        List<Dosen> dosenList = dosenRepository.findAll();
        List<DosenDto> dosenDto =
                dosenList.stream()
                         .map(dosen -> mapDosenToDosenDto(dosen))
                         .collect(Collectors.toList());
        return  dosenDto;
    }

    @GetMapping("/{id}")
    public DosenDto getBiodata(@PathVariable Integer id) {
        Dosen dosen = dosenRepository.findById(id).get();
        DosenDto dosenDto = new DosenDto();
        modelMapper.map(dosen, dosenDto);
        modelMapper.map(dosen.getTipeDosen(), dosenDto);
        modelMapper.map(dosen.getJurusan(), dosenDto);
//        dosenDto.setIdTipeDosen(dosen.getTipeDosen().getId());
//        dosenDto.setIdJurusan(dosen.getJurusan().getId());
        dosenDto.setIdDosen(dosen.getId());
        return  dosenDto;
    }

    private DosenDto mapDosenToDosenDto(Dosen dosen) {
        DosenDto dosenDto = modelMapper.map(dosen, DosenDto.class);
        dosenDto.setJurusan(dosen.getJurusan().getJurusan());
        dosenDto.setTipeDosen(dosen.getTipeDosen().getTipeDosen());
//        modelMapper.map(dosen.getJurusan(), dosenDto);
//        modelMapper.map(dosen.getTipeDosen(), dosenDto);
        dosenDto.setIdJurusan(dosen.getJurusan().getId());
        dosenDto.setIdTipeDosen(dosen.getTipeDosen().getId());
        dosenDto.setIdDosen(dosen.getId());
        return dosenDto;
    }

    @PostMapping()
    public DosenDto editSaveData(@RequestBody DosenDto dosenDto) {
        Dosen dosen = modelMapper.map(dosenDto, Dosen.class);
        dosen.setIdTipeDosen(dosenDto.getIdTipeDosen());
        dosen.setIdJurusan(dosenDto.getIdJurusan());
        dosen.setId(dosenDto.getIdDosen());
        dosen = dosenService.saveDosenMaterDetail(dosen);
        DosenDto dosenDtoDB = mapDosenToDosenDto(dosen);
        return dosenDtoDB;
    }

    @DeleteMapping("/{id}")
    public void delelete(@PathVariable Integer id) {
        dosenRepository.deleteById(id);
    }

    @DeleteMapping
    @ResponseBody
    public void deleteTable() {
        dosenRepository.deleteAll();
    }

}
