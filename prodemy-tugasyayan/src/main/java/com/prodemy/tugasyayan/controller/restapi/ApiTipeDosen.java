package com.prodemy.tugasyayan.controller.restapi;

import com.prodemy.tugasyayan.model.entity.TipeDosen;
import com.prodemy.tugasyayan.repository.TipeDosenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/tipe-dosen")
public class ApiTipeDosen {
    @Autowired
    private TipeDosenRepository tipeDosenRepository;

    @GetMapping()
    public List<TipeDosen> getAll() {
        return tipeDosenRepository.findAll();
    }
}
