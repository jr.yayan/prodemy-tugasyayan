package com.prodemy.tugasyayan.controller.restapi;

import com.prodemy.tugasyayan.model.entity.Agama;
import com.prodemy.tugasyayan.repository.AgamaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/agama")
public class ApiAgama {
    @Autowired
    private AgamaRepository agamaRepository;

    @GetMapping()
    public List<Agama> getAll() {
        return agamaRepository.findAll();
    }
}
