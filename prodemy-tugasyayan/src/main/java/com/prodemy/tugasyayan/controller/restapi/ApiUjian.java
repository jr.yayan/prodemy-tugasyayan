package com.prodemy.tugasyayan.controller.restapi;

import com.prodemy.tugasyayan.model.entity.Ujian;
import com.prodemy.tugasyayan.repository.UjianRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/ujian")
public class ApiUjian {
    @Autowired
    private UjianRepository ujianRepository;

    @GetMapping()
    public List<Ujian> getAll() {
        return ujianRepository.findAll();
    }
}
