var tableBiodata = {
  create: function () {
    // jika table tersebut datatable, maka clear and dostroy
    if ($.fn.DataTable.isDataTable('#tableBiodata')) {
      //table yg sudah dibentuk menjadi datatable harus d rebuild lagi untuk di instantiasi ulang
      $('#tableBiodata').DataTable().clear();
      $('#tableBiodata').DataTable().destroy();
    }

    $.ajax({
      url: '/api/dosen',
      method: 'get',
      contentType: 'application/json',
      success: function (res, status, xhr) {
        if (xhr.status == 200 || xhr.status == 201) {
          $('#tableBiodata').DataTable({
            data: res,
            columns: [
              {
                title: "Nama",
                data: "dosen"
              },
              {
                title: "Jurusan",
                data: "jurusan"
              },
              {
                title: "Tipe Dosen",
                data: "tipeDosen"
              },

              {
                title: "Action",
                data: null,
                render: function (data, type, row) {
                  return "<button class='btn-success' data-toggle='tooltip' title='edit' data-placement='bottom' onclick=formBiodata.setEditData('" + data.idDosen + "') style='border-radius: 20%; margin-right: 5px;'><i class='fa fa-pencil-alt'></i></button>" +
                    "<button class='btn-danger' data-toggle='tooltip' title='delete' data-placement='bottom' onclick=actionDelete.deleteConfirm('" + data.idDosen + "') style='border-radius: 20%'><i class='fa fa-minus-circle'></i></button>"
                }
              }
            ],
            scrollX : true
          });

        } else {

        }
      },
      error: function (err) {
        console.log(err);
      }
    });
  }
};

var formBiodata = {
  resetForm: function () {
    $('#form-biodata')[0].reset();
    $('#idDosen').val("");
  },

  saveForm: function () {
    if ($('#form-biodata').parsley().validate()) {
      var dataResult = getJsonForm($("#form-biodata").serializeArray(), true);
      $.ajax({
        url: '/api/dosen',
        method: 'post',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(dataResult),
        success: function (res, status, xhr) {
          if (xhr.status == 200 || xhr.status == 201) {
            tableBiodata.create();
            $('#modal-biodata').modal('hide')

          } else {

          }
        },
        erorrr: function (err) {
          console.log(err);
        }
      });
    }
  },

  setEditData: function (id) {
    formBiodata.resetForm();

    $.ajax({
      url: '/api/dosen/' + id,
      method: 'get',
      contentType: 'application/json',
      dataType: 'json',
      success: function (res, status, xhr) {
        if (xhr.status == 200 || xhr.status == 201) {
          $('#form-biodata').fromJSON(JSON.stringify(res));
          $('#modal-biodata').modal('show')

        } else {

        }
      },
      erorrr: function (err) {
        console.log(err);
      }
    });
  }
};

var actionDelete = {
  deleteConfirm: function (id) {
    $.ajax({
      url: '/api/dosen/' + id,
      method: 'get',
      contentType: 'application/json',
      dataType: 'json',
      success: function (res, status, xhr) {
        if (xhr.status == 200 || xhr.status == 201) {
          $('#form-biodata').fromJSON(JSON.stringify(res));
          $('#modal-delete').modal('show')
        } else {

        }
      },
      erorrr: function (err) {
        console.log(err);
      }
    });
  },

  deleteRowData : function () {
    if ($('#form-biodata').parsley().validate()) {
      var dataResult = getJsonForm($("#form-biodata").serializeArray(), true);

      $.ajax({
        url: '/api/dosen/' + dataResult.idDosen,
        method: 'delete',
        success: function () {
          tableBiodata.create();
          $('#modal-delete').modal('hide');
        },
        erorrr: function (err) {
          console.log(err);
        }
      });
    }
  },

  deleteTable : function () {
    $.ajax({
      url: '/api/dosen',
      method: 'delete',
      success: function () {
        tableBiodata.create()
        $('#modal-delete').modal('hide')
      }

    })

  }


};
